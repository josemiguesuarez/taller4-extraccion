package crawler;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Locale;

import javax.print.attribute.standard.PrinterInfo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crowler {

	public static void main(String[] args) {
		try {
			Document doc = Jsoup
					.connect("https://play.google.com/store/apps/category/FINANCE/collection/topselling_paid")
					.timeout(0).get();

			HashSet<String> hrefs = new HashSet<String>();
			Elements anchors = doc.getElementsByClass("card-click-target");
			for (Element element : anchors) {
				hrefs.add("https://play.google.com" + element.attr("href").toString());
			}

			double scoreTotalFinanzas = 0;
			int numAplicacones = 0;
			for (String url : hrefs) {
				doc = Jsoup.connect(url).timeout(10000).get();

				printInfo(doc, "Nombre", "id-app-title");
				printInfo(doc, "Rating Count", "rating-count");
				String calificacion = printInfo(doc, "Calificación", "score");
				printInfoBySelector(doc, "Descripción", "[itemprop='description']");
				printInfo(doc, "Cambios recientes", "recent-change");
				printInfo(doc, "Ratings con 5 estrellas", "rating-bar-container five");
				printInfo(doc, "Ratings con 4 estrellas", "rating-bar-container four");

				numAplicacones++;
				NumberFormat ukFormat = NumberFormat.getNumberInstance(Locale.FRANCE);

				scoreTotalFinanzas += ukFormat.parse(calificacion).doubleValue();

			}
			System.out.println("**** Calificación promedio de aplicaciones de finanza con mejores ventas: "
					+ (scoreTotalFinanzas / numAplicacones));

			// System.out.println(element);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static String printInfo(Document doc, String name, String className) {
		Element element = doc.getElementsByClass(className).first();
		return printIt(name, element);
	}

	private static String printInfoBySelector(Document doc, String name, String selector) {
		Element element = doc.select(selector).first();
		return printIt(name, element);
	}

	private static String printIt(String name, Element element) {
		String text = "*Sin " + name;
		if (element != null) {
			text = element.text();
		}
		System.out.println(name + ": " + text);
		return text;
	}

}
